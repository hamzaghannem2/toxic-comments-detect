# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 19:21:10 2021

@author: DNCCR
"""
from prep import *
import re
import nltk
# nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
# nltk.download('wordnet') 
from nltk.stem.wordnet import WordNetLemmatizer
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas
from sklearn.model_selection import train_test_split
from sklearn.metrics.pairwise import cosine_similarity
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import confusion_matrix
import json
import nltk
from flask import Flask, render_template, request,jsonify
import numpy as np
import random
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd









df_train=pd.read_excel("c:\\Users\\DNCCR\\Music\\toxic.xlsx")
df_train=df_train[['text_comment','target']]


for i in range(len(df_train)):
    if df_train['target'][i]==1:
        df_train['target'][i]='toxic'
    else:
        df_train['target'][i]='not toxic'





ttt=list(df_train['text_comment'])



def LemTokens(tokens):
    return [nltk.stem.WordNetLemmatizer().lemmatize(token) for token in tokens]

remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

def LemNormalize(text):
    return nltk.word_tokenize(text.lower().translate(remove_punct_dict))


stop_words= set(stopwords.words("english"))

#print(stop_words)
##Creating a list of custom stopwords
new_words = ["using", "show", "result", "large",'by','year','university','experience','an','intern','phone', "also",'en','on','ma','un','une','moins','de','la','le','et','du','de','les','des', "if", "one", "two", "new", "previously", "shown"]
stop_words = stop_words.union(new_words)
# corpus = []
# def clean(df,corpus):
#     for i in range(0, len(df)):
#         #Remove punctuations
#         text = re.sub('[^a-zA-Z]',' ', str(df['text_comment'][i]))

        
#         #Convert to lowercase
#         text = text.lower()
        
#         #remove tags
#         text=re.sub("&lt;/?.*?&gt;"," &lt;&gt; ",text)
        
#         # remove special characters and digits
#         text=re.sub("(\\d+|\\W)+"," ",text)
        
#         ##Convert to list from string
#         text = text.split()
        
#         ##Stemming
#         ps=PorterStemmer()
# #        Lemmatisation
#         lem = WordNetLemmatizer()
#         text = [lem.lemmatize(word) for word in text if not word in  
#                 stop_words] 
#         text = " ".join(text)
#         corpus.append(text)
# clean(df_train,corpus)
# ttt=corpus
def response(userText):
    robo_response = ''
    ttt.append(userText)
    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english', ngram_range=(1, 2), max_features=300)
    features = TfidfVec.fit_transform(ttt)
    cosine = cosine_similarity(features[-1], features)
    best = cosine.argsort()[0][-2]
    flat = cosine.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if (req_tfidf == 0):
        robo_response = robo_response + "Not toxic"
        ttt.remove(userText)
        return robo_response
    else:
        robo_response = robo_response + df_train['target'][df_train.index[df_train['text_comment'] == ttt[best]].tolist()]
        ttt.remove(userText)
        return robo_response

response("well this what i want ")
