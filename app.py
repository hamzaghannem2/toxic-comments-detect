

from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify
from flask import redirect
from flask import url_for
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import numpy as np
import joblib
import re
import nltk
# nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
# nltk.download('wordnet') 
from nltk.stem.wordnet import WordNetLemmatizer
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas
from sklearn.model_selection import train_test_split
from sklearn.metrics.pairwise import cosine_similarity
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import confusion_matrix
import json
import nltk
from flask import Flask, render_template, request,jsonify
import numpy as np
import random
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd



df_train=pd.read_excel("c:\\Users\\DNCCR\\Music\\toxic.xlsx")
df_train=df_train[['text_comment','target']]


ttt=list(df_train['text_comment'])



def LemTokens(tokens):
    return [nltk.stem.WordNetLemmatizer().lemmatize(token) for token in tokens]

remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

def LemNormalize(text):
    return nltk.word_tokenize(text.lower().translate(remove_punct_dict))


stop_words= set(stopwords.words("english"))

#print(stop_words)
##Creating a list of custom stopwords
new_words = ["using", "show", "result", "large",'by','year','university','experience','an','intern','phone', "also",'en','on','ma','un','une','moins','de','la','le','et','du','de','les','des', "if", "one", "two", "new", "previously", "shown"]
stop_words = stop_words.union(new_words)
app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html" )

@app.route("/api", methods=["GET","POST"])
def api():
    if request.method == "POST":
        tweet = request.form["tweet"]
        userText=tweet
        robo_response = ''
        ttt.append(userText)
        TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english', ngram_range=(1, 2), max_features=300)
        features = TfidfVec.fit_transform(ttt)
        cosine = cosine_similarity(features[-1], features)
        best = cosine.argsort()[0][-2]
        flat = cosine.flatten()
        flat.sort()
        req_tfidf = flat[-2]
        if (req_tfidf == 0):
            robo_response = robo_response + "Not toxic"
            ttt.remove(userText)
            msg="Ce Tweet est classé comme étant Non Toxique"
            return render_template("index.html", msg=msg, tweet=tweet)
        else:
            robo_response = robo_response + df_train['target'][df_train.index[df_train['text_comment'] == ttt[best]].tolist()]
            ttt.remove(userText)
            msg= "Ce Tweet est classé comme étant Toxique"
            return render_template("index.html", msg=msg, tweet=tweet)
    else:
        return redirect(url_for("index"))
    
if __name__ == '__main__':
    app.run(debug=True)